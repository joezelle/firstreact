import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import Nav from "./components/Nav/Nav";
import Testimonials from "./components/Testimonials/Testimonials";
import Menu from "./components/Menu/Menu";
import HeaderText from "./components/HeaderText/HeaderText";
import Footer from "./components/Footer/Footer";
import Main from "./components/Main/Main";
import * as serviceWorker from "./serviceWorker";
import Background from "./images/bread-bun-burger-1639562.jpg";

ReactDOM.render(
  <div className="FullPage">
    <header
      className="header"
      style={{
        backgroundImage: `url(${Background})`,
        backgroundPosition: "center",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        height: "100vh",
        width: "100%",
        position: "relative"
      }}
    >
      <div
        style={{
          backgroundColor: "rgba(0,0,0,0.2)"
        }}
      >
        <div
          style={{
            width: "80%",
            margin: "0 auto"
          }}
        >
          <Nav />
          <HeaderText />
        </div>
      </div>
    </header>
    <div className="container">
      <Main />
      <Menu />
    </div>
    <Testimonials />
    <Footer />
  </div>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
