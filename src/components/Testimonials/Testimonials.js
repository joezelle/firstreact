import React from "react";
import "./Testimonials.css";

const Article = function() {
  return (
    <div className="Testimonials">
      <div
        style={{
          backgroundColor: "rgba(0,0,0,0.6)",
          height: "50vh"
        }}
      >
        <div
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)"
          }}
        >
          <h2
            style={{
              fontSize: "30px",
              marginBottom: "30px"
            }}
          >
            See What Our Customers Are Saying
          </h2>
          <p>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quas ea
            deserunt, dignissimos, odio atque doloremque incidunt sapiente
            laborum velit itaque ipsam? Asperiores mollitia veniam facere!
            Voluptas dolorum quae iusto quo beatae alias amet, eum sed
            consectetur iure ipsa sequi minima?
          </p>
        </div>
      </div>
    </div>
  );
};

export default Article;
