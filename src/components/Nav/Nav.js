import React from "react";
import "./Nav.css";

const Nav = function() {
  return (
    <div>
      <nav className="Nav">
        <h2
          style={{
            color: "orange"
          }}
        >
          DINER
        </h2>
        <ul>
          <li>Home</li>
          <li>Menu</li>
          <li>About</li>
          <li
            style={{
              backgroundColor: "orange",
              borderRadius: "6px"
            }}
          >
            ORDER NOW
          </li>
        </ul>
      </nav>
    </div>
  );
};
export default Nav;
