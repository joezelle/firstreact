import React from "react";
import "./Footer.css";

export default function Footer() {
  return (
    <footer>
      <div className="footer">
        <h3
          style={{
            color: "orange",
            fontSize: "30px",
            flex: "0 0 70%"
          }}
        >
          DINER
        </h3>

        <div
          className="newsletter"
          style={{
            display: "flex",
            flexDirection: "column"
          }}
        >
          <input type="text" />
          <button>Subcribe to Our Newsletter</button>
        </div>
      </div>
      <p>Copyright Diner 2019</p>
    </footer>
  );
}
