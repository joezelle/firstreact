import React from "react";
import img1 from "../../images/appetizer-bun-burger-2283567.jpg";
import img2 from "../../images/beef-bread-bun-552056.jpg";
import img3 from "../../images/burger-chips-dinner-70497.jpg";
import "./Menu.css";

export default function SideBar() {
  return (
    <div>
      <h2
        style={{
          fontSize: "30px",
          marginBottom: "10px"
        }}
      >
        See Our Sumptous Menu
      </h2>
      <div
        style={{
          display: "flex",
          textAlign: "center"
        }}
      >
        <div
          style={{
            margin: "5px 20px "
          }}
        >
          <img src={img1} alt="burger" width="100%" />
          <h5>Lorem</h5>
        </div>
        <div
          style={{
            margin: "5px 20px"
          }}
        >
          <img src={img2} alt="burger" width="100%" />
          <h5>Lorem</h5>
        </div>
        <div
          style={{
            margin: "5px 20px"
          }}
        >
          <img src={img3} alt="burger" width="100%" />
          <h5>Lorem</h5>
        </div>
      </div>
      <div
        style={{
          textAlign: "center"
        }}
      >
        <h3
          style={{
            backgroundColor: "orange",
            borderRadius: "6px",
            display: "inline-block",
            alignSelf: "center",
            padding: "15px 25px",
            color: "white",
            margin: "30px 0"
          }}
        >
          Order Now
        </h3>
      </div>
    </div>
  );
}
