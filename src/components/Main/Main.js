import React from "react";
import "./Main.css";

export default function Main() {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        marginTop: "50px"
      }}
    >
      <div className="Main">
        <h2
          style={{
            fontSize: "30px"
          }}
        >
          WELCOME TO DINER
        </h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde quod
          harum sunt laborum laudantium id. Illum placeat, hic doloremque,
          adipisci excepturi nisi, consequuntur quas veniam voluptatibus odit
          maiores ex ea aliquid? Dolor, hic!
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde quod
          harum reiciendis itaque et quas in provident rem autem, nulla, culpa
          commodi odit venint laborum laudantium id. Illum placeat, hic
          doloremque, adipisci excepturi nisi, consequuntur quas veniam
          voluptatibus odit maiores ex ea aliquid? Dolor, hic!
        </p>
      </div>

      <div
        style={{
          width: "100%"
        }}
      >
        <iframe
          style={{
            flex: "0 0 30%",
            width: "100%",
            height: "90%"
          }}
          src="https://www.youtube.com/embed/oqeW9YMHweo"
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
          title="video"
        />
      </div>
    </div>
  );
}
