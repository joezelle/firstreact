import React from "react";

export default function HeaderText() {
  return (
    <div
      style={{
        position: "absolute",
        top: "50%",
        left: "15%",
        width: "500px",
        fontSize: "60px",
        transform: "translate(-10%, -50%)",
        color: "white"
      }}
    >
      <h3>
        When Life Throws you a{" "}
        <span
          style={{
            padding: "3px 10px",
            color: "orange"
          }}
        >
          Burger,
        </span>
        Eat It
      </h3>
    </div>
  );
}
